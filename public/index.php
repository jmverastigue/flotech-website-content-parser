<?php
include_once('../simple_html_dom.php');

error_reporting(0);

function seoUrl($string) {
	//Lower case everything
	$string = strtolower($string);
	//Make alphanumeric (removes all other characters)
	$string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
	//Clean up multiple dashes or whitespaces
	$string = preg_replace("/[\s-]+/", " ", $string);
	//Convert whitespaces and underscore to dash
	$string = preg_replace("/[\s_]/", "_", $string);
	return $string;
}

function replaceImagePath($string){
	$string = str_replace("/images/stories/", "/img/products/", $string);
	
	return $string;
}

function removeMce($string){
	$string = preg_replace('/(<[^>]+) mce_style=".*?"/i', '$1', $string);
	$string = preg_replace('/(<[^>]+) mce_src=".*?"/i', '$1', $string);
	$string = preg_replace('/(<[^>]+) mce_href=".*?"/i', '$1', $string);
	
	return $string;
}

if(isset($_POST['url'])){
	$url=$_POST['url'];
}
else{
	$url="http://www.flotech.com.sg/products/flow/variable-area/226-miniature-glass-tube-variable-area-flowmeter.html";
}

$html = file_get_html($url);

foreach($html->find('h1[class=rt-article-title]',0) as $element)
{
	if($element->plaintext !="")
 	{
 		$title = trim($element->plaintext);
 		$slug = seoUrl($title);
 	}
}

$body="";
foreach($html->find('.rt-article',0)->children() as $element)
{
		
	if($element->tag=="p")
	{
		if($element->find('.smoothness')){
			break;		
		}
		$body .= replaceImagePath($element->outertext);
	}
}

$body = trim(removeMce($body));
$features = trim(removeMce($html->find("div[id^=magictabs]",1)->innertext));
$applications = trim(removeMce($html->find("div[id^=magictabs]",2)->innertext));
$specifications = trim(removeMce($html->find("div[id^=magictabs]",3)->innertext));
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Flotech Website Content Parser</title>

<link href="css/bootstrap.min.css" rel="stylesheet">

<style type="text/css">
.inputwidth {
	width: 40%;
}
</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<form class="form-horizontal" action="" method="post">
				<div class="form-group">
					<label class="col-md-2 control-label">URL</label>
					<div class="col-md-9">
						<input name="url" type="text" class="form-control"
							value="<?php echo $url?>"><br>
					</div>
					<div class="col-md-1">
						<input type="submit" class="form-control">
					</div>
				</div>
			</form>
			<form class="form-horizontal">
				<div class="form-group">
					<label class="col-md-2 control-label">Title</label>
					<div class="col-md-10">
						<input type="text" class="form-control inputwidth"
							value="<?php echo $title?>"><br>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">Slug</label>
					<div class="col-md-10">
						<input type="text" class="form-control inputwidth"
							value="<?php echo $slug?>"><br>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">Body</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="10">
						<?php echo $body?>
					</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">Features</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="10">
						<?php echo $features?>
					</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">Applications</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="10">
						<?php echo $applications?>
					</textarea>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-2 control-label">Specifications</label>
					<div class="col-md-10">
						<textarea class="form-control" rows="10">
						<?php echo $specifications?>
					</textarea>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>